import 'package:flutter/material.dart';
import 'package:learn_flutter/src/utils/routes.dart';

void main() {
  runApp(MaterialApp(
    title: 'Flutter - Learn',
    theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'AirBnb',
        appBarTheme: const AppBarTheme(
            titleTextStyle: TextStyle(fontWeight: FontWeight.w700, fontSize: 18)
        )
    ),
    initialRoute: '/',
    onGenerateRoute: RouteGenerator.generateRoute,
    debugShowCheckedModeBanner: false,
  ));
}
