import 'package:flutter/cupertino.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class WithoutOverScrollBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}