
import 'package:flutter/material.dart';
import 'package:learn_flutter/src/screens/LayoutSample1Page.dart';
import 'package:learn_flutter/src/screens/SampleApp.dart';

import '../screens/AsyncApp.dart';
import '../screens/AsyncIsolateApp.dart';
import '../screens/DemoApp.dart';
import '../screens/FadeAppTest.dart';
import '../screens/FormInputApp.dart';
import '../screens/GestureApp.dart';
import '../screens/LayoutBasicApp.dart';
import '../screens/ListViewExample.dart';
import '../screens/SharedPreferencesApp.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return _createRoute(const SampleAppPage());
      case '/signature':
        return _createRoute(const SignatureApp());
      case '/fade':
        return _createRoute(const FadeAppTest());
      case '/async':
        return _createRoute(const AsyncApp());
      case '/async_isolate':
        return _createRoute(const AsyncIsolateApp());
      case '/gesture':
        return _createRoute(const GestureApp(title: 'Gesture Example'));
      case '/listview':
        return _createRoute(const ListViewExample());
      case '/form_input':
        return _createRoute(const FormInputApp(title: 'Form Input'));
      case '/shared_preferences':
        return _createRoute(const SharedPreferencesApp(title: 'Shared Preferences'));
      case '/layout_basic':
        return _createRoute(const LayoutBasicApp());
      case '/layout_sample_one':
        return _createRoute(const LayoutSample1Page());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_)  {
      return Scaffold(
        appBar: AppBar(title: const Text('Error')),
        body: const Center(child: Text('Error page')),
      );
    });
  }

  static Route _createRoute(Widget page) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        const curve = Curves.ease;
        
        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
      transitionDuration: const Duration(milliseconds: 1000)
    );
  }
}