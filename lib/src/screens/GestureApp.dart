
import 'package:flutter/material.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class GestureApp extends StatefulWidget {
  const GestureApp({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<GestureApp> createState() => _GestureAppState();
}

class _GestureAppState extends State<GestureApp> with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late CurvedAnimation curve;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2000),
        vsync: this,
    );
    curve = CurvedAnimation(
        parent: controller,
        curve: Curves.easeIn
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: GestureDetector(
          onDoubleTap: () {
            if (controller.isCompleted) {
              controller.reverse();
            } else {
              controller.forward();
            }
          },
          child: RotationTransition(
              turns: curve,
              child: const FlutterLogo(
                size: 200.0,
              ),
          ),
        )
      ),
    );
  }

}