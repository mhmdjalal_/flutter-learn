
import 'package:flutter/material.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class FormInputApp extends StatefulWidget {
  const FormInputApp({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<FormInputApp> createState() => _FormInputAppState();
}

class _FormInputAppState extends State<FormInputApp> {
  String? _errorText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: TextField(
          onSubmitted: (text) {
            setState(() {
              if(!isEmail(text.trim())) {
                _errorText = 'Error: This is not an email';
              } else {
                _errorText = null;
              }
            });
          },
          decoration: InputDecoration(
            hintText: 'This is a hint',
            errorText: _getErrorText()
          ),
        )
      ),
    );
  }

  String? _getErrorText() {
    return _errorText;
  }

  bool isEmail(String text) {
    String emailRegexp =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|'
        r'(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|'
        r'(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = RegExp(emailRegexp);

    return regExp.hasMatch(text);
  }

}