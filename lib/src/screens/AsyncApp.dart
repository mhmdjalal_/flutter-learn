import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

/// @author Created by Muhamad Jalaludin on 15/08/2022

class AsyncApp extends StatelessWidget {
  const AsyncApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const Scaffold(body: AsyncAppPage());

}

class AsyncAppPage extends StatefulWidget {
  const AsyncAppPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AsyncAppPageState();

}

class _AsyncAppPageState extends State<AsyncAppPage> {

  List widgets = [];

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Async App'),
      ),
      body: ListView.builder(
        itemCount: widgets.length,
        itemBuilder: (context, position) {
          return getRow(position);
        },
      ),
    );
  }

  Widget getRow(int i) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Text("Row ${widgets[i]["title"]}"),
    );
  }

  Future<void> loadData() async {
    var dataUrl = Uri.parse("https://jsonplaceholder.typicode.com/posts");
    http.Response response = await http.get(dataUrl);
    setState(() {
      widgets = jsonDecode(response.body);
    });
  }

}