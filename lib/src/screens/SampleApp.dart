import 'package:flutter/material.dart';
import 'package:learn_flutter/src/utils/Strings.dart';

/// @author Created by Muhamad Jalaludin on 11/08/2022
class SampleAppPage extends StatefulWidget {
  const SampleAppPage({Key? key}) : super(key: key);

  @override
  State<SampleAppPage> createState() => _SampleAppPageState();

}

class _SampleAppPageState extends State<SampleAppPage> {

  void _onItemTapped(int index) {
    setState(() {
      switch (index) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.appName),
      ),
      body: Scaffold(
        body: Center(
          child: Column(
            children: [
              CustomButton("Signature", () {
                Navigator.of(context).pushNamed('/signature');
              }),
              CustomButton("Fade", () {
                Navigator.of(context).pushNamed('/fade');
              }),
              CustomButton("Async", () {
                Navigator.of(context).pushNamed('/async');
              }),
              CustomButton("Async Isolate", () {
                Navigator.of(context).pushNamed('/async_isolate');
              }),
              CustomButton("Gesture Detector", () {
                Navigator.of(context).pushNamed('/gesture');
              }),
              CustomButton("ListView", () {
                Navigator.of(context).pushNamed('/listview');
              }),
              CustomButton("Form Input", () {
                Navigator.of(context).pushNamed('/form_input');
              }),
              CustomButton("Shared Preferences", () {
                Navigator.of(context).pushNamed('/shared_preferences');
              }),
              CustomButton("Layout Basic", () {
                Navigator.of(context).pushNamed('/layout_basic');
              }),
              CustomButton("Layout Sample 1", () {
                Navigator.of(context).pushNamed('/layout_sample_one');
              })
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Dashboard",
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.home_work),
                label: "Kandang"
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.newspaper),
                label: "Komunitas"
            )
          ],
        onTap: _onItemTapped,
      ),
    );
  }

}

class CustomButton extends StatelessWidget {
  final String label;
  final VoidCallback? listener;

  const CustomButton(this.label, this.listener, {super.key});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: listener,
      child: Text(label),
    );
  }
}