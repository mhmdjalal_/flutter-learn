
import 'package:flutter/material.dart';
import 'package:learn_flutter/src/widgets/widgets.dart';

import '../utils/utils.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class LayoutBasicApp extends StatefulWidget {
  const LayoutBasicApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LayoutBasicAppState();

}

class _LayoutBasicAppState extends State<LayoutBasicApp> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Widgets.customAppBar('Layout Basic'),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: ScrollConfiguration(
          behavior: WithoutOverScrollBehavior(),
          child: ListView(
            children: [
              CustomTitle(title: 'Example: Row'),
              Row(
                children: const [
                  BlueBox(),
                  BlueBox(),
                  BlueBox(),
                ],
              ),
              CustomTitle(title: 'Example: Column'),
              Column(
                children: const [
                  BlueBox(),
                  BlueBox(),
                  BlueBox(),
                ],
              ),
              CustomTitle(title: 'Example: MainAxisSize', desc: 'Row and Column occupy different main axes. A Row’s main axis is horizontal, and a Column’s main axis is vertical. The mainAxisSize property determines how much space a Row and Column can occupy on their main axes.'),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  BlueBox(),
                  BlueBox(),
                  BlueBox(),
                ],
              ),
              CustomTitle(title: 'Example: MainAxisAlignment', desc: 'When mainAxisSize is set to MainAxisSize.max, Row and Column might lay out their children with extra space. The mainAxisAlignment property determines how Row and Column can position their children in that extra space.'),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  BlueBox(),
                  BlueBox(),
                  BlueBox(),
                ],
              ),
              CustomTitle(title: 'Example: CrossAxisAlignment', desc: 'The crossAxisAlignment property determines how Row and Column can position their children on their cross axes. A Row’s cross axis is vertical, and a Column’s cross axis is horizontal.'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  BlueBox(),
                  BiggerBlueBox(),
                  BlueBox(),
                ],
              ),
              CustomTitle(title: 'Example: Flexible Widget', desc: 'The Flexible widget wraps a widget, so the widget becomes resizable. When the Flexible widget wraps a widget, the widget becomes the Flexible widget’s child and is considered flexible. After inflexible widgets are laid out, the widgets are resized according to their flex and fit properties.'),
              Row(
                children: const [
                  BlueBox(),
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: BlueBox()
                  ),
                  Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: BlueBox()
                  )
                ],
              ),
              Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    children: const [
                      BlueBox(),
                      Flexible(
                          fit: FlexFit.tight,
                          flex: 2,
                          child: BlueBox()
                      ),
                      Flexible(
                          fit: FlexFit.tight,
                          flex: 1,
                          child: BlueBox()
                      )
                    ],
                  )
              ),
              CustomTitle(title: 'Example: Expanded Widget', desc: 'Similar to Flexible, the Expanded widget can wrap a widget and force the widget to fill extra space.'),
              Row(
                children: const [
                  BlueBox(),
                  Expanded(child: BlueBox()),
                  BlueBox()
                ],
              ),
              CustomTitle(title: 'Example: SizedBox Widget', desc: 'The SizedBox widget can be used in one of two ways when creating exact dimensions. When SizedBox wraps a widget, it resizes the widget using the height and width properties. When it doesn’t wrap a widget, it uses the height and width properties to create empty space.'),
              Row(
                children: const [
                  BlueBox(),
                  SizedBox(
                      width: 100,
                      height: 100,
                      child: BlueBox()
                  ),
                  BlueBox()
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: const [
                    BlueBox(),
                    SizedBox(width: 50),
                    BlueBox(),
                    SizedBox(width: 50),
                    BlueBox()
                  ],
                ),
              ),
              CustomTitle(title: 'Example: Spacer Widget', desc: 'Similar to SizedBox, the Spacer widget also can create space between widgets.'),
              Row(
                children: const [
                  BlueBox(),
                  Spacer(flex: 1),
                  BlueBox(),
                  Spacer(flex: 1),
                  BlueBox()
                ],
              ),
              CustomTitle(title: 'Example: Text Widget', desc: 'The Text widget displays text and can be configured for different fonts, sizes, and colors.'),
              Row(
                crossAxisAlignment: CrossAxisAlignment.baseline,
                textBaseline: TextBaseline.alphabetic,
                children: const [
                  Text(
                    'Hey!',
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.blue
                    ),
                  ),
                  Text(
                    'Hey!',
                    style: TextStyle(
                        fontSize: 50,
                        color: Colors.green
                    ),
                  ),
                  Text(
                    'Hey!',
                    style: TextStyle(
                        fontSize: 40,
                        color: Colors.red
                    ),
                  )
                ],
              ),
              CustomTitle(title: 'Example: Icon Widget', desc: 'The Icon widget displays a graphical symbol that represents an aspect of the UI.'),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                textBaseline: TextBaseline.alphabetic,
                children: const [
                  Icon(
                    Icons.widgets,
                    size: 50,
                    color: Colors.blue,
                  ),
                  Icon(
                    Icons.widgets,
                    size: 50,
                    color: Colors.red,
                  )
                ],
              ),
              CustomTitle(title: 'Example: Image Widget', desc: 'The Image widget displays an image. You either can reference images using a URL, or you can include images inside your app package.'),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network('https://gitlab.com/uploads/-/system/user/avatar/2757590/avatar.png'),
                ],
              ),
              CustomTitle(title: 'Example: Business Card'),
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.black),
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        const Icon(
                          Icons.account_circle,
                          size: 55,
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text('Muhamad Jalaludin', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
                            Text('Mobile Developer', style: TextStyle(fontWeight: FontWeight.w400))
                          ],
                        )
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text('Bogor, Indonesia', style: TextStyle(fontWeight: FontWeight.w400)),
                        Text('0812-9120-3843', style: TextStyle(fontWeight: FontWeight.w400))
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: const [
                        Icon(Icons.accessibility),
                        Icon(Icons.timer),
                        Icon(Icons.phone_android),
                        Icon(Icons.phone_iphone),
                      ],
                    )
                  ],
                )
              )
            ],
          ),
        )
      ),
    );
  }

}

class BlueBox extends StatelessWidget {
  const BlueBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
        color: Colors.blue,
        border: Border.all()
      ),
    );
  }
}

class BiggerBlueBox extends StatelessWidget {
  const BiggerBlueBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 100,
      decoration: BoxDecoration(
        color: Colors.blue,
        border: Border.all()
      ),
    );
  }
}
