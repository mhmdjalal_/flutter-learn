
import 'package:flutter/material.dart';

/// @author Created by Muhamad Jalaludin on 11/08/2022
class FadeAppTest extends StatelessWidget {
  const FadeAppTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const Scaffold(body: MyFadeTest(title: 'Fade Demo'));

}

class MyFadeTest extends StatefulWidget {
  const MyFadeTest({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyFadeTest> createState() => _MyFadeTest();
}

class _MyFadeTest extends State<MyFadeTest> with TickerProviderStateMixin {
  late AnimationController controller;
  late CurvedAnimation curve;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 2000),
        vsync: this,
    );
    curve = CurvedAnimation(
        parent: controller,
        curve: Curves.easeIn
    );

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: FadeTransition(
          opacity: curve,
          child: const FlutterLogo(
            size: 100.0,
          ),
        ),
      ),
    );
  }

}