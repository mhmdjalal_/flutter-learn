import 'package:flutter/material.dart';

/// @author Created by Muhamad Jalaludin on 17/08/2022

class CustomTitle extends StatelessWidget {
  String title;
  String? desc;

  CustomTitle({Key? key, required this.title, this.desc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(title, style: const TextStyle(fontWeight: FontWeight.w500), textAlign: TextAlign.start),
          if (desc != null) Padding(padding: const EdgeInsets.only(top: 10), child: Text(desc ?? '', style: const TextStyle(fontWeight: FontWeight.w300), textAlign: TextAlign.justify))
        ]
      ),
    );
  }
}

class Widgets {
  static PreferredSizeWidget customAppBar(String title) {
    return AppBar(
      title: Text(title),
    );
  }
}
